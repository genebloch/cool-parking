﻿namespace CoolParking.WebAPI.Helpers
{
    public class VehiclePostService
    {
        public bool IsBadRequst(string json)
        {
            if (json.Split(',').Length != 3)
            {
                return true;
            }

            if (json.Contains("id") && json.Contains("vehicleType") && json.Contains("balance"))
            {
                return false;
            }

            return true;
        }
    }
}