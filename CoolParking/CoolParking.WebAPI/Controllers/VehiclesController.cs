﻿using System;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parking;

        public VehiclesController(IParkingService parking)
        {
            _parking = parking;
        }

        [HttpGet]
        public IActionResult GetVehicles()
        {
            var vehicles = _parking.GetVehicles().ToList();

            return Ok(vehicles);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(string id)
        {
            bool isValid = Vehicle.IdValidation(id);

            if (!isValid)
            {
                return BadRequest("Id is invalid");
            }

            var vehicle = _parking.GetVehicles().FirstOrDefault(v => v.Id == id);

            if (vehicle == null)
            {
                return NotFound("Vehicle not found");
            }
            
            return Ok(vehicle);
        }

        [HttpPost]
        public IActionResult AddVehicle([FromBody]System.Text.Json.JsonElement body)
        {
            string json = System.Text.Json.JsonSerializer.Serialize(body);

            if (!new VehiclePostService().IsBadRequst(json))
            {
                try
                {
                    Vehicle vehicle = Newtonsoft.Json.JsonConvert.DeserializeObject<Vehicle>(json);

                    _parking.AddVehicle(vehicle);

                    return Created($"{vehicle.Id}", vehicle);
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }

            return BadRequest("Request body is invalid");
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteVehicele(string id)
        {
            bool isValid = Vehicle.IdValidation(id);

            if (!isValid)
            {
                return BadRequest("Id is invalid");
            }

            var vehicle = _parking.GetVehicles().FirstOrDefault(v => v.Id == id);

            if (vehicle == null)
            {
                return NotFound(vehicle);
            }

            _parking.RemoveVehicle(id);

            return NoContent();
        }
    }
}