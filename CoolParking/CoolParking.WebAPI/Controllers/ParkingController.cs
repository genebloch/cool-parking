﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parking;

        public ParkingController(IParkingService parking)
        {
            _parking = parking;
        }

        [HttpGet("balance")]
        public IActionResult GetBalance()
        {
            var balance = _parking.GetBalance();

            return Ok(balance);
        }

        [HttpGet("capacity")]
        public IActionResult GetCapacity()
        {
            var capacity = _parking.GetCapacity();

            return Ok(capacity);
        }

        [HttpGet("freePlaces")]
        public IActionResult GetFreePlaces()
        {
            var freePlaces = _parking.GetFreePlaces();

            return Ok(freePlaces);
        }
    }
}