﻿using System;
using System.Linq;
using CoolParking.BL.Helpers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parking;

        public TransactionsController(IParkingService parking)
        {
            _parking = parking;
        }

        [HttpGet("last")]
        public IActionResult GetLastTransactions()
        {
            var lastTransactions = _parking.GetLastParkingTransactions();

            return Ok(lastTransactions);
        }

        [HttpGet("all")]
        public IActionResult GetAllTransactions()
        {
            string transactions;

            try
            {
                transactions = _parking.ReadFromLog();
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
            
            return Ok(transactions);
        }

        [HttpPut("topUpVehicle")]
        public IActionResult TopUpVehicle([FromBody]Payment payment)
        {
            try
            {
                Vehicle vehicle = _parking.GetVehicles().FirstOrDefault(v => v.Id == payment.Id);

                _parking.TopUpVehicle(payment.Id, payment.Sum);

                return Ok(vehicle);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}