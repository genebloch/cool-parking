﻿using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; set; }
        public VehicleType VehicleType { get; set; }
        public decimal Balance { get; set; }

        public Vehicle() { }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (IdValidation(id) && balance > -1)
            {
                Id = id;
                VehicleType = vehicleType;
                Balance = balance;
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public override string ToString()
        {
            return $"{Id}\n{VehicleType}\n{Balance}";
        }

        public static bool IdValidation(string id)
        {
            Regex regex = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
            Match match = regex.Match(id);

            if (match.Value == "")
            {
                return false;
            }
            
            return true;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var id = string.Empty;

            Random rand = new Random();

            id += GenerateRandomLetter(rand);
            id += GenerateRandomLetter(rand);
            id += "-";
            id += rand.Next(1000, 9999).ToString();
            id += "-";
            id += GenerateRandomLetter(rand);
            id += GenerateRandomLetter(rand);

            return id;
        }

        private static char GenerateRandomLetter(Random rand)
        {
            char A = 'A';
            char Z = 'Z';

            return (char)rand.Next(A, Z + 1);
        }
    }
}