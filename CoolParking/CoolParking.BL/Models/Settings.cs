﻿namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal InitialParkingBalance { get; private set; } = 0;
        public static int ParkingCapacity { get; private set; } = 10;
        public static int WithdrawInterval { get; private set; } = 5000;
        public static int LogInterval { get; private set; } = 60000;

        public static decimal GetTariff(VehicleType vehicleType)
        {
            decimal tariff = 0m;

            switch (vehicleType)
            {
                case VehicleType.Bus:
                    tariff = 3.5m;
                    break;
                case VehicleType.Motorcycle:
                    tariff = 1m;
                    break;
                case VehicleType.PassengerCar:
                    tariff = 2m;
                    break;
                case VehicleType.Truck:
                    tariff = 5m;
                    break;
            }

            return tariff;
        }

        public static decimal PenaltyRatio { get; private set; } = 2.5m;
    }
}