﻿namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public string Time { get; private set; }
        public string Id { get; private set; }
        public decimal Sum { get; private set; }

        public TransactionInfo(string time, string id, decimal sum)
        {
            Time = time;
            Id = id;
            Sum = sum;
        }

        public override string ToString()
        {
            return $"{Id}\n{Sum}\n{Time}";
        }
    }
}