﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking _parking;

        public decimal Balance { get; set; }
        public List<Vehicle> Vehicles { get; private set; }

        public static Parking GetParking()
        {
            if (_parking == null)
            {
                _parking = new Parking();
            }

            return _parking;
        }

        private Parking()
        {
            Vehicles = new List<Vehicle>();
        }
    }
}