﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ILogService _logService;
        private readonly List<TransactionInfo> _transactions;
        private readonly Parking _parking;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _transactions = new List<TransactionInfo>();
            _parking = Parking.GetParking();
            _logService = logService;

            Dispose();

            withdrawTimer.Interval = Settings.WithdrawInterval;
            withdrawTimer.Elapsed += Withdraw;

            logTimer.Interval = Settings.LogInterval;
            logTimer.Elapsed += Log;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (!IsUnique(vehicle.Id) || !Vehicle.IdValidation(vehicle.Id))
            {
                throw new ArgumentException();
            }

            if (_parking.Vehicles.Count < Settings.ParkingCapacity)
            {
                _parking.Vehicles.Add(vehicle);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public void Dispose()
        {
            _parking.Vehicles.Clear();
            _parking.Balance = Settings.InitialParkingBalance;
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.ParkingCapacity;
        }

        public int GetFreePlaces()
        {
            return Settings.ParkingCapacity - _parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_parking.Vehicles);
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);

            if (vehicle == null)
            {
                throw new ArgumentException();
            }

            if (vehicle.Balance < 0)
            {
                throw new InvalidOperationException();
            }

            _parking.Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);

            if (vehicle == null || sum < 0)
            {
                throw new ArgumentException();
            }

            vehicle.Balance += sum;
        }

        private bool IsUnique(string id)
        {
            bool present = _parking.Vehicles.Any(v => v.Id == id);

            if (present)
            {
                return false;
            }

            return true;
        }

        private void Withdraw(object source, ElapsedEventArgs e)
        {
            decimal total = 0;

            foreach(Vehicle vehicle in _parking.Vehicles)
            {
                decimal sum = Settings.GetTariff(vehicle.VehicleType);

                if (vehicle.Balance <= 0)
                {
                    sum *= Settings.PenaltyRatio;
                }
                else if (vehicle.Balance - sum < 0)
                {
                    decimal remainder = sum - vehicle.Balance;

                    sum = vehicle.Balance + remainder * Settings.PenaltyRatio;
                }

                _transactions.Add(new TransactionInfo(DateTime.Now.ToString("h:mm:ss tt"), vehicle.Id, sum));

                total += sum;
                vehicle.Balance -= sum;
            }

            _parking.Balance += total;
        }

        private void Log(object source, ElapsedEventArgs e)
        {
            var log = string.Empty;

            foreach (TransactionInfo transaction in _transactions)
            {
                log += transaction.ToString() + "\n";
            }

            _logService.Write(log);

            _transactions.Clear();
        }
    }
}