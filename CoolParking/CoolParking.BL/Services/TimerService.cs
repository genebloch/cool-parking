﻿using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer _timer;

        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            _timer.Dispose();
        }

        public void Start()
        {
            _timer = new Timer(Interval);

            _timer.Elapsed += OnTimedEvent;

            _timer.AutoReset = true;
            _timer.Enabled = true;
        }

        public void Stop()
        {
            _timer.Stop();
        }

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            Elapsed?.Invoke(source, e);
        }
    }
}