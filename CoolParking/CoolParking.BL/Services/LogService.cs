﻿using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; private set; }

        public LogService(string path)
        {
            LogPath = path;
        }

        public string Read()
        {
            if (File.Exists(LogPath))
            {
                return File.ReadAllText(LogPath);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public void Write(string logInfo)
        {
            File.AppendAllText(LogPath, logInfo + "\n");
        }
    }
}