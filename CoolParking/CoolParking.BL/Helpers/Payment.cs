﻿namespace CoolParking.BL.Helpers
{
    public class Payment
    {
        public string Id { get; set; }
        public decimal Sum { get; set; }
    }
}