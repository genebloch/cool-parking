﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Helpers;
using CoolParking.BL.Models;
using Newtonsoft.Json;

namespace CoolParking.PL
{
    public class Menu
    {
        private static string _base;
        private static HttpClient _client;

        public Menu()
        {
            _base = "https://localhost:44343/api/";
            _client = new HttpClient();
        }

        public async Task RunAsync()
        {
            await ShowAvailableOperationAsync();
        }

        private async Task ShowAvailableOperationAsync()
        {
            Console.WriteLine("1: Show parking balance");
            Console.WriteLine("2: Show free places");
            Console.WriteLine("3: Show parking capacity");
            Console.WriteLine("4: Show current transactions");
            Console.WriteLine("5: Show transactions history");
            Console.WriteLine("6: Show all vehicles");
            Console.WriteLine("7: Add new vehicle");
            Console.WriteLine("8: Remove vehicle");
            Console.WriteLine("9: Top up balance");
            Console.WriteLine("10: Get vehicle by id");

            await ChooseFunctionAsync(ReceiveCommand());
        }

        private int ReceiveCommand()
        {
            Console.Write("> ");

            int command = -1;

            try
            {
                command = int.Parse(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return command;
        }

        private async Task ChooseFunctionAsync(int command)
        {
            switch (command)
            {
                case 1:
                    await ShowParkingBalanceAsync();
                    await GoBackAsync();
                    break;
                case 2:
                    await ShowFreePlacesAsync();
                    await GoBackAsync();
                    break;
                case 3:
                    await ShowParkingCapacityAsync();
                    await GoBackAsync();
                    break;
                case 4:
                    await ShowCurrentTransactionsAsync();
                    await GoBackAsync();
                    break;
                case 5:
                    await ShowTransactionsHistoryAsync();
                    await GoBackAsync();
                    break;
                case 6:
                    await ShowAllVehiclesAsync();
                    await GoBackAsync();
                    break;
                case 7:
                    await AddNewVehicleAsync();
                    await GoBackAsync();
                    break;
                case 8:
                    await RemoveVehicleAsync();
                    await GoBackAsync();
                    break;
                case 9:
                    await TopUpBalanceAsync();
                    await GoBackAsync();
                    break;
                case 10:
                    await GetVehiclesById();
                    await GoBackAsync();
                    break;
                default:
                    Console.WriteLine("Invalid command");
                    await ShowAvailableOperationAsync();
                    break;
            }
        }

        private async Task GoBackAsync()
        {
            Console.WriteLine("Press any button to go back");
            Console.ReadKey();
            Console.Clear();
            await ShowAvailableOperationAsync();
        }

        private async Task ShowParkingBalanceAsync()
        {
            HttpResponseMessage response = await _client.GetAsync($"{_base}parking/balance");
            response.EnsureSuccessStatusCode();
            var balance = await response.Content.ReadAsStringAsync();

            Console.WriteLine($"Balance: {balance}");
        }

        private async Task ShowFreePlacesAsync()
        {
            HttpResponseMessage response = await _client.GetAsync($"{_base}parking/freePlaces");
            response.EnsureSuccessStatusCode();
            string freePlaces = await response.Content.ReadAsStringAsync();

            Console.WriteLine($"Free places: {freePlaces}");
        }

        private async Task ShowParkingCapacityAsync()
        {
            HttpResponseMessage response = await _client.GetAsync($"{_base}parking/capacity");
            response.EnsureSuccessStatusCode();
            var capacity = await response.Content.ReadAsStringAsync();

            Console.WriteLine($"Parking capacity: {capacity}");
        }

        private async Task ShowCurrentTransactionsAsync()
        {
            HttpResponseMessage response = await _client.GetAsync($"{_base}transactions/last");
            response.EnsureSuccessStatusCode();
            var transactions = await response.Content.ReadAsStringAsync();

            Console.WriteLine(transactions);
        }

        private async Task ShowTransactionsHistoryAsync()
        {
            HttpResponseMessage response = await _client.GetAsync($"{_base}transactions/all");
            response.EnsureSuccessStatusCode();
            var transactions = await response.Content.ReadAsStringAsync();

            try
            {
                Console.WriteLine(transactions);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task GetVehiclesById()
        {
            Console.Write("Id: ");
            string id = Console.ReadLine();

            try
            {
                HttpResponseMessage response = await _client.GetAsync($"{_base}vehicles/{id}");
                response.EnsureSuccessStatusCode();
                var vehicle = await response.Content.ReadAsStringAsync();

                Console.WriteLine(vehicle);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task ShowAllVehiclesAsync()
        {
            HttpResponseMessage response = await _client.GetAsync($"{_base}vehicles");
            response.EnsureSuccessStatusCode();
            var vehicles = await response.Content.ReadAsStringAsync();

            Console.WriteLine(vehicles);
        }

        private async Task AddNewVehicleAsync()
        {
            string id = Vehicle.GenerateRandomRegistrationPlateNumber();

            try
            {
                Console.Write("Vehicle type (PassengerCar, Truck, Bus, Motorcycle): ");
                var type = (VehicleType)Enum.Parse(typeof(VehicleType), Console.ReadLine());

                Console.Write("Balance: ");
                var balance = int.Parse(Console.ReadLine());

                Vehicle vehicle = new Vehicle(id, type, balance);
                string json = JsonConvert.SerializeObject(vehicle);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = await _client.PostAsync($"{_base}vehicles", content);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task RemoveVehicleAsync()
        {
            Console.Write("Id: ");
            string id = Console.ReadLine();

            try
            {
                HttpResponseMessage response = await _client.DeleteAsync($"{_base}vehicles/{id}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task TopUpBalanceAsync()
        {
            Console.Write("Id: ");
            string id = Console.ReadLine();

            try
            {
                Console.Write("Sum: ");
                int sum = int.Parse(Console.ReadLine());

                Payment payment = new Payment() { Id = id, Sum = sum };

                string json = JsonConvert.SerializeObject(payment);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = await _client.PutAsync($"{_base}transactions/topUpVehicle", content);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}