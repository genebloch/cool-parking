﻿using System.Threading.Tasks;

namespace CoolParking.PL
{
    class Program
    {
        static async Task Main()
        {
            await new Menu().RunAsync();
        }
    }
}